<?php
/**
 * Created by PhpStorm.
 * User: jetsaus
 * Date: 19.09.2018
 * Time: 23:50
 *
 * Примеры работы с куками
 *
 */


// Установим куку
setcookie("user", "Admin", time() + (3600 * 24 * 7));

// Удалим куку, установив ей время жизни в прошлое
//setcookie("user", "Admin", time() - (3600 * 24 * 7));

// Проверим существование
if (isset($_COOKIE["user"])) {
    $user = $_COOKIE["user"];
} else {
    $user = "Гость";
}

// Выведем куку в браузер
echo "Привет, {$user}";
